/* Brandon Baker
 * CS 201
 * Assignment #3
 * graph.h
 * [description]
 */
#include <stdio.h>
#include "darray.h"
#include "binomial.h"

#ifndef ASS3_GRAPH_H
#define ASS3_GRAPH_H

typedef struct vertex {
	int id, key, steps;
	struct vertex *predecessor;
	BinomialNode *position;
	DArray *neighbors;
} vertex;

typedef struct neighbor {
	vertex *vert;
	int weight;
} neighbor;

typedef struct adjlist {
	vertex **vertices;
	int size;
} adjlist;

extern vertex *newVertex(int id);
extern neighbor *newNeighbor(int vertid, int weight);
extern adjlist *newAdjList(FILE *input);
extern void populateAdjList(adjlist *list, FILE *input);
extern void dijkstra(adjlist *list);

extern void printCrudeList(adjlist *list);

//extern void populateGraph(graph *g, FILE *input);

#endif //ASS3_GRAPH_H
