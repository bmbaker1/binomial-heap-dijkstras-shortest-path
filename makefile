objects = sll.o queue.o utils.o scanner.o darray.o binomial.o graph.o main.o

oopts = -Wall -Wextra -std=c99 -g -c
lopts = -Wall -Wextra -std=c99 -g

all: dijkstra

dijkstra: $(objects)
	gcc $(lopts) -o dijkstra $(objects)

sll.o: sll.c sll.h
	gcc $(oopts) sll.c

queue.o: queue.c queue.h sll.h
	gcc $(oopts) queue.c

utils.o: utils.c utils.h
	gcc $(oopts) utils.c

scanner.o: scanner.c scanner.h
	gcc $(oopts) scanner.c

darray.o: darray.c darray.h
	gcc $(oopts) darray.c

binomial.o: binomial.c binomial.h
	gcc $(oopts) binomial.c

graph.o: graph.c graph.h darray.h binomial.h
	gcc $(oopts) graph.c

main.o: main.c utils.h graph.h
	gcc $(oopts) main.c

clean:
	rm -f $(objects) dijkstra

make test:
	./dijkstra testFiles/g6.0 > myoutput/my6
	diff testFiles/outG6.txt myoutput/my6
	./dijkstra testFiles/g7.0 > myoutput/my7
	diff testFiles/outG7.txt myoutput/my7
	./dijkstra testFiles/g8.0 > myoutput/my8
	diff testFiles/outG8.txt myoutput/my8
	./dijkstra testFiles/g9.0 > myoutput/my9
	diff testFiles/outG9.txt myoutput/my9
	./dijkstra testFiles/g10.0 > myoutput/my10
	diff testFiles/outG10.txt myoutput/my10
	./dijkstra testFiles/g11.0 > myoutput/my11
	diff testFiles/outG11.txt myoutput/my11
	./dijkstra testFiles/g12.0 > myoutput/my12
	diff testFiles/outG12.txt myoutput/my12
	./dijkstra testFiles/g13.0 > myoutput/my13
	diff testFiles/outG13.txt myoutput/my13
	./dijkstra testFiles/g14.0 > myoutput/my14
	diff testFiles/outG14.txt myoutput/my14
	./dijkstra testFiles/g15.0 > myoutput/my15
	diff testFiles/outG15.txt myoutput/my15
	./dijkstra testFiles/single.0 > myoutput/mySingle
	diff testFiles/outSingle.txt myoutput/mySingle
	./dijkstra testFiles/two.0 > myoutput/myTwo
	diff testFiles/outTwo.txt myoutput/myTwo
	./dijkstra testFiles/unweighted.0 > myoutput/myUnweighted
	diff testFiles/outUnweighted.txt myoutput/myUnweighted
	rm myoutput/*
