/* Brandon Baker
 * CS 201
 * Assignment #3
 * main.c
 */

#include <stdio.h>
#include "utils.h"
#include "graph.h"


int main(int argc, char **argv) {

	if(argc != 2)
		fatal("Not enough program arguments... Pleas run program as such: "
					"djikstra [input]");

	FILE *input = fopen(argv[1], "r");

	//todo: fix makefile dependencies for everything...

	adjlist *list = newAdjList(input);
	populateAdjList(list, input);

	dijkstra(list);

	return 0;
}