/* Brandon Baker
 * CS 201
 * Assignment #1
 * decimal.c
 */

#include "integer.h"
#include <stdlib.h>
#include "utils.h"

/* Public Interface Implementations */

integer *newInteger(int v) {
	integer *newdecimal;
	if((newdecimal = malloc(sizeof(integer))) == 0)
		fatal("Integer not allocated");

	newdecimal->value = v;

	return newdecimal;
}

int getInteger(integer *this) {
	return this->value;
}

int compareInteger(void *this, void *that) {
	if(!this && !that) return 0;
	if(!this) return -1;
	if(!that) return 1;

	return(getInteger(this) - getInteger(that));
}

void displayInteger(FILE *out, void *value) {
	fprintf(out, "%d", getInteger(value));
}

