/* Brandon Baker
 * CS 201
 * Assignment #3
 * darray.c
 */

#include "darray.h"
#include <stdlib.h>
#include <stdarg.h>

/* Private Variables */

struct DArray {
	void **array;
	int capacity, size;
	void (*display)(FILE *, void *);
};


/* Private Method Prototypes */

static void fatalDArray(char *message, ...);

static int dArrayIsFull(DArray *arr);
static int needsToShrink(DArray *arr);
static DArray *reallocateDArray(DArray *arr);


/* Private Method Implementations */

void fatalDArray(char *message, ...) {
	va_list ap;

	fprintf(stderr, "Error: ");
	va_start(ap, message);
	vfprintf(stderr, message, ap);
	vfprintf(stderr, "... Exiting\n", ap);
	va_end(ap);

	exit(666);
}

int dArrayIsFull(DArray *arr) {
	return(arr->size == arr->capacity);
}

int needsToShrink(DArray *arr) {
	return arr->size < (arr->capacity / 4);
}

DArray *reallocateDArray(DArray *arr) {
	if(needsToShrink(arr)) arr->capacity /= 2;
	else arr->capacity *= 2;

	void **temp = realloc(arr->array, arr->capacity * sizeof(arr->array));

	if(temp) arr->array = temp;
	else fatalDArray("Array not reallocated correctly");

	return arr;
}


/* Public Interface Implementations */

DArray *newDArray(void (*display)(FILE *, void *)) {
	DArray *newarr;
	if((newarr = malloc(sizeof(DArray))) == 0)
		fatalDArray("Dynamic array not allocated");

	newarr->array = malloc(1 * sizeof(newarr->array));
	newarr->capacity = 1;
	newarr->size = 0;
	newarr->display = display;

	return newarr;
}

void insertDArray(DArray *arr, void *value) {
	//todo: check that the reallocation stuff works on linux!!!
	if(dArrayIsFull(arr)) arr = reallocateDArray(arr);

	arr->size++;
	arr->array[arr->size - 1] = value;
}

void *removeDArray(DArray *arr) {
	if(arr->size == 0) fatalDArray("Removing from an impty array");

	int last = arr->size - 1;
	void *temp = arr->array[last];

	arr->array[last] = NULL;
	arr->size--;

	if(needsToShrink(arr)) reallocateDArray(arr);

	return temp;
}

void *getDArray(DArray *arr, int index) {
//	printf("index: %d, arr->size: %d\n", index, arr->size);
	if(index < arr->size) return arr->array[index];
	return NULL;
}

void setDArray(DArray *arr, int index, void *value) {
	//todo: how to handle out of bounds...
	if(index == arr->size) insertDArray(arr, value);
	else arr->array[index] = value;
}

int sizeDArray(DArray *arr) {
	return arr->size;
}

void displayDArray(FILE *fp, DArray *arr) {
//	fprintf(fp, "Array Capacity: %d\n", arr->capacity);
//	fprintf(fp, "Array Size: %d\n", sizeDArray(arr));

	fprintf(fp, "[");
	for(int i = 0; i < arr->size; i++) {
		arr->display(fp, arr->array[i]);

		if(i != arr->size - 1) fprintf(fp, ",");
	}
	fprintf(fp, "][%d]", arr->capacity - arr->size);
}
