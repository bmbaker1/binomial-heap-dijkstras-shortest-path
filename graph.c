/* Brandon Baker
 * CS 201
 * Assignment #3
 * graph.c
 */

#include "graph.h"
#include <stdlib.h>
#include <string.h>
#include <limits.h>
#include "scanner.h"
#include "queue.h"
#include "utils.h"

/* Private Method Prototypes */

static int setMaxVert(int maxvert, int vertone, int verttwo);
static int getMaxVertices(FILE *input);
static vertex **allocateVerticesList(size_t size);
static int isLineEnd(char *token);
static void addVertex(adjlist *list, int id);
static void updateVertices(adjlist *list, int vert1, int vert2);
static void addNeighbor(vertex *vert, int neighborid, int weight);
static void updateNeighbor(vertex *vert, int neighborid, int weight);
static void updateNeighbors(adjlist *list, int vert1, int vert2, int weight);

/* Dijkstra Stuff */

static void displayVertex(FILE *fp, void *value);
static int compareKeys(void *this, void *that);
static void updateVertex(void *v, BinomialNode *node);
static int getSourceIndex(adjlist *list);
static void setSourceVertex(adjlist *list);
static Binomial *addVerticesToBinomial(adjlist *list);
static int isOrphan(vertex *vert);
static int getEdgeWeight(vertex *u, vertex *v);


/* Private Method Implementations */

int setMaxVert(int maxvert, int vertone, int verttwo) {
	if(maxvert > vertone && maxvert > verttwo) return maxvert;
	else return (vertone > verttwo) ? vertone : verttwo;
}

int getMaxVertices(FILE *input) {
	int linecount = 0, maxvert = -1, start = -1, end = -1;
	char *token = readToken(input);

	while(!feof(input)) {
		while(!isLineEnd(token)) {
			if(linecount == 0) start = atoi(token);
			if(linecount == 1) end = atoi(token);
			linecount++;
			token = readToken(input);
		}
		maxvert = setMaxVert(maxvert, start, end);
		linecount = 0;
		token = readToken(input);
	}
	rewind(input);
	return maxvert;
}

vertex **allocateVerticesList(size_t size) {
	vertex **list;
	if((list = (vertex **)malloc(sizeof(vertex *) * size)) == 0)
		fatal("Vertices List not Allocated");

	for(unsigned int i = 0; i < size; i++) {
		list[i] = NULL;
	}

	return list;
}

int isLineEnd(char *token) {
	return strcmp(token, ";") == 0;
}

void addVertex(adjlist *list, int id) {
	list->vertices[id] = newVertex(id);
}

void updateVertices(adjlist *list, int vert1, int vert2) {
	if(!list->vertices[vert1]) addVertex(list, vert1);
	if(!list->vertices[vert2]) addVertex(list, vert2);
}

void addNeighbor(vertex *vert, int neighborid, int weight) {
	neighbor *newbor = newNeighbor(neighborid, weight);
	insertDArray(vert->neighbors, newbor);
}

void updateNeighbor(vertex *vert, int neighborid, int weight) {
	neighbor *currneighbor;

	for(int i = 0; i < sizeDArray(vert->neighbors); i++) {
		currneighbor = getDArray(vert->neighbors, i);

		if(currneighbor->vert->id == neighborid) {
			if(currneighbor->weight > weight)
				currneighbor->weight = weight;
			return;
		}
	}

	addNeighbor(vert, neighborid, weight);
}

void updateNeighbors(adjlist *list, int vert1, int vert2, int weight) {
	updateNeighbor(list->vertices[vert1], vert2, weight);
	updateNeighbor(list->vertices[vert2], vert1, weight);
}

/* Dijkstra Stuff */

void displayVertex(FILE *fp, void *value) {
	vertex *vert = value;
	fprintf(fp, "%d", vert->id);

	if(vert->predecessor)
		fprintf(fp, "(%d)%d", vert->predecessor->id, vert->key);
}

int compareKeys(void *this, void *that) {
	if(!this && !that) return 0;
	if(!this) return -1;
	if(!that) return 1;

	vertex *vthis = this, *vthat = that;

	if(vthis->key == vthat->key) return vthis->id - vthat->id;
	return (vthis->key - vthat->key);
}

int compareDijkstra(void *this, void *that) {
	if(!this && !that) return 0;
	if(!this) return -1;
	if(!that) return 1;

	vertex *vthis = this, *vthat = that;

	if(vthis->steps == vthat->steps) {
		if(vthis->key == vthat->key) return vthis->id - vthat->id;
		return vthis->key - vthat->key;
	}

	return vthis->steps - vthat->steps;
}

void updateVertex(void *v, BinomialNode *node) {
	vertex *vert = v;
	vert->position = node;
}

int getSourceIndex(adjlist *list) {
	int i;

	for(i = 0; i < list->size; i++)
		if(list->vertices[i]) break;

	return i;
}

void setSourceVertex(adjlist *list) {
	int sourceindex = getSourceIndex(list);
	list->vertices[sourceindex]->key = 0;
}

Binomial *addVerticesToBinomial(adjlist *list) {
	Binomial *vertheap = newBinomial(displayVertex,
									 compareKeys,
									 updateVertex);
	vertex *vert;

	for(int i = 0; i < list->size; i++) {
		vert = list->vertices[i];

		if(list->vertices[i])
			list->vertices[i]->position = insertBinomial(vertheap, vert);
	}

	return vertheap;
}

int isOrphan(vertex *vert) {
	return vert->predecessor == NULL;
}

void sortQueue(queue *q) {
	Binomial *bino = newBinomial(displayVertex,
								 compareDijkstra,
								 updateVertex);

	while(sizeQueue(q) > 0) insertBinomial(bino, dequeue(q));
	while(sizeBinomial(bino) > 0) enqueue(q, extractBinomial(bino));
}

int getEdgeWeight(vertex *u, vertex *v) {
	neighbor *bor;

	for(int i = 0; i < sizeDArray(u->neighbors); i++) {
		bor = getDArray(u->neighbors, i);
		if(bor->vert->id == v->id) return bor->weight;
	}

	return 0; // Should never return this
}

void displayVisitedVertices(queue *q) {
	vertex *curr, *next = NULL;

	sortQueue(q);

	printf("0 :");

	if(sizeQueue(q) > 0) printf(" ");

	while(sizeQueue(q) > 0) {
		curr = dequeue(q);
		next = NULL;

		if(sizeQueue(q) > 0) next = peekQueue(q);

		displayVertex(stdout, curr);

		if(!next) break;

		if(curr->steps != next->steps) printf("\n%d : ", next->steps);
		else printf(" ");
	}

	printf("\n----\n");
}


/* Public Interface Implementations */

vertex *newVertex(int id) {
	vertex *newvert;
	if((newvert = malloc(sizeof(vertex))) == 0)
		fatal("Vertex not allocated");

	newvert->id = id;
	newvert->key = INT_MAX;
	newvert->steps = 0;
	newvert->predecessor = NULL;
	newvert->position = NULL;
	newvert->neighbors = newDArray(NULL);

	return newvert;
}

neighbor *newNeighbor(int vertid, int weight) {
	neighbor *newbor;
	if((newbor = malloc(sizeof(neighbor))) == 0)
		fatal("Neighbor not allocated");

	newbor->vert = newVertex(vertid);
	newbor->weight = weight;

	return newbor;
}

adjlist *newAdjList(FILE *input) {
	adjlist *newlist;
	if((newlist = malloc(sizeof(adjlist))) == 0)
		fatal("AdjList not allocated");

	newlist->size = getMaxVertices(input) + 1;
	newlist->vertices = allocateVerticesList((size_t)newlist->size);

	return newlist;
}

void populateAdjList(adjlist *list, FILE *input) {
	int linecount = 0, vert1 = -1, vert2 = -1, weight = -1;
	char *token = readToken(input);

	while(!feof(input)) {
		while(!isLineEnd(token)) {
			if(linecount == 0) vert1 = atoi(token);
			if(linecount == 1) vert2 = atoi(token);
			if(linecount == 2) weight = atoi(token);
			else weight = 1;
			linecount++;
			token = readToken(input);
		}
		updateVertices(list, vert1, vert2);
		updateNeighbors(list, vert1, vert2, weight);

		linecount = 0;
		token = readToken(input);
	}
}

void dijkstra(adjlist *list) {
	vertex *u, *v;
	neighbor *bor;
	queue *visited = newQueue(displayVertex);
	int weight;

	setSourceVertex(list);

	Binomial *vertbino = addVerticesToBinomial(list);

	while(sizeBinomial(vertbino) > 0) {
		u = extractBinomial(vertbino);

		if(!u->predecessor) {
			u->key = 0;

			if(sizeQueue(visited) > 0) displayVisitedVertices(visited);
		}

		enqueue(visited, u);

		for(int i = 0; i < sizeDArray(list->vertices[u->id]->neighbors); i++) {
			bor = getDArray(u->neighbors, i);
			v = list->vertices[bor->vert->id];

			if(v) {
				if(v->key == 0) continue;

				weight = getEdgeWeight(u, v);

				if(isOrphan(v) || u->key + weight < v->key) {
					v->predecessor = u;
					v->key = u->key + weight;
					v->steps = v->predecessor->steps + 1;
					decreaseKeyBinomial(vertbino, v->position, v);
				}
			}
		}
	}
	displayVisitedVertices(visited);
}

void printCrudeList(adjlist *list) {
	neighbor *neitex;

	for(int i = 0; i < list->size; i++) {
		if(list->vertices[i]){
			printf("%d -> ", list->vertices[i]->id);
			for(int j = 0; j < sizeDArray(list->vertices[i]->neighbors); j++) {
				neitex = getDArray(list->vertices[i]->neighbors, j);
				if(neitex){
					printf("%d(%d) ", neitex->vert->id, neitex->weight);
				}
			}
		}
		else printf("NULL\n");
		if(list->vertices[i]) newline();
	}
}
