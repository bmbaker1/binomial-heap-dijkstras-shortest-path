/* Brandon Baker
 * CS 201
 * Assignment #3
 * binomial.h
 * [description]
 */

#ifndef ASS3_BINOMIAL_H
#define ASS3_BINOMIAL_H

#include <stdio.h>

typedef struct BinomialNode BinomialNode;

BinomialNode *newBinomialNode(void (*display)(FILE *, void *), void *value);
void displayBinomialNode(FILE *fp, BinomialNode *binode);


typedef struct Binomial Binomial;

extern Binomial *newBinomial(void (*display)(FILE *, void *),
							 int (*comparator)(void *, void *),
							 void (*updater)(void *, BinomialNode *));
extern BinomialNode *insertBinomial(Binomial *bino, void *value);
extern int sizeBinomial(Binomial *bino);
extern void deleteBinomial(Binomial *bino, BinomialNode *binode);
extern void decreaseKeyBinomial(Binomial *bino, BinomialNode *binode,
								void *value);
extern void *extractBinomial(Binomial *bino);
extern void displayBinomial(FILE *fp, Binomial *bino);

extern void testSomeShit(Binomial *bino);

#endif //ASS3_BINOMIAL_H
