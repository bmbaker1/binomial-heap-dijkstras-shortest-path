/* Brandon Baker
 * CS 201
 * Assignment #3
 * darray.h
 * Header for darray module, which is a module that creates and maintains
 * a dynamic array.
 */

#ifndef ASS3_DARRAY_H
#define ASS3_DARRAY_H

#include <stdio.h>

typedef struct DArray DArray;

extern DArray *newDArray(void (*display)(FILE *, void *));
extern void insertDArray(DArray *arr, void *value);
extern void *removeDArray(DArray *arr);
extern void *getDArray(DArray *arr, int index);
extern void setDArray(DArray *arr, int index, void *value);
extern int sizeDArray(DArray *arr);
extern void displayDArray(FILE *, DArray *arr);

#endif //ASS3_DARRAY_H
