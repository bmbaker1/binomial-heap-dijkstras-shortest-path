/* Brandon Baker
 * CS 201
 * Assignment #1
 * decimal.h
 * [description]
 */
#ifndef ASS1_DECIMAL_H
#define ASS1_DECIMAL_H

#include <stdio.h>

typedef struct integer {
	int value;
} integer;

extern integer *newInteger(int);
extern int getInteger(integer *);
extern int compareInteger(void *, void *);
extern void displayInteger(FILE *, void *);

#endif //ASS1_DECIMAL_H
