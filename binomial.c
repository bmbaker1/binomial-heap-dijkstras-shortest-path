/* Brandon Baker
 * CS 201
 * Assignment #3
 * binomial.c
 */

#include "binomial.h"
#include <stdlib.h>
#include <stdarg.h>
#include "darray.h"
#include "queue.h"

/* Private Variables */

struct BinomialNode {
	void *value;
	DArray *children;
	BinomialNode *parent;
	void (*display)(FILE *, void *);
};

struct Binomial {
	DArray *rootlist;
	BinomialNode *extreme;
	int size;
	void (*display)(FILE *, void *);
	int (*compare)(void *, void *);
	void (*update)(void *, BinomialNode *);
};


/* Private Method Prototypes */

static void fatalBinomial(char *message, ...);
static void *combine(Binomial *bino, BinomialNode *x, BinomialNode *y);
static BinomialNode *grabHeap(DArray *rootlist, int index);
static void consolidate(Binomial *bino, BinomialNode *binode);
static void merge(Binomial *bino, DArray *donor);
static void swapBinomialNodeValues(BinomialNode *this, BinomialNode *that);
static BinomialNode *bubbleUp(Binomial *bino, BinomialNode *binode);
static int findIndexInRootlist(Binomial *bino, BinomialNode *y);
static BinomialNode *findNewExtreme(Binomial *bino);
static void addRootToQueue(queue *q, BinomialNode *binode);
static void printRowInfo(FILE *fp, int rowcount);


/* Private Method Implementations */

void fatalBinomial(char *message, ...) {
	va_list ap;

	fprintf(stderr, "Error: ");
	va_start(ap, message);
	vfprintf(stderr, message, ap);
	vfprintf(stderr, "... Exiting\n", ap);
	va_end(ap);

	exit(666);
}

static void *combine(Binomial *bino, BinomialNode *x, BinomialNode *y) {
	int i;

	if(bino->compare(x->value, y->value) < 0) {
		i = sizeDArray(y->children);
		setDArray(x->children, i, y);
		y->parent = x;
		return x;
	}
	else {
		i = sizeDArray(x->children);
		setDArray(y->children, i, x);
		x->parent = y;
		return y;
	}
}

BinomialNode *grabHeap(DArray *rootlist, int index) {
	if(index > sizeDArray(rootlist)) fatalBinomial("Cannot grab heap");

	if(index == sizeDArray(rootlist)) return NULL;
	else return getDArray(rootlist, index);
}

void consolidate(Binomial *bino, BinomialNode *binode) {
	int degree = sizeDArray(binode->children);

	while(grabHeap(bino->rootlist, degree)) {
		binode = combine(bino, binode, getDArray(bino->rootlist, degree));
		setDArray(bino->rootlist, degree, NULL);
		degree++;
	}
	setDArray(bino->rootlist, degree, binode);
}

void merge(Binomial *bino, DArray *donor) {
	BinomialNode *binode;

	for(int i = 0; i < sizeDArray(donor); i++) {
		binode = getDArray(donor, i);
		binode->parent = binode;
		consolidate(bino, binode);
	}
	free(donor);
}

void swapBinomialNodeValues(BinomialNode *this, BinomialNode *that) {
	void *temp = this->value;
	this->value = that->value;
	that->value = temp;
}

BinomialNode *bubbleUp(Binomial *bino, BinomialNode *binode) {
	if(binode == binode->parent) return binode;
	else if(bino->compare(binode->value, binode->parent->value) > 0)
		return binode;
	else {
		if(bino->update) {
			bino->update(binode->value, binode->parent);
			bino->update(binode->parent->value, binode);
		}
		swapBinomialNodeValues(binode, binode->parent);

		return bubbleUp(bino, binode->parent);
	}
}

int findIndexInRootlist(Binomial *bino, BinomialNode *binode) {
	int size = sizeDArray(bino->rootlist), i;
	BinomialNode *temp;

	for(i = 0; i < size; i++) {
		temp = getDArray(bino->rootlist, i);
		if(temp)
			if(bino->compare(temp->value, binode->value) == 0) break;
	}

	return i;
}

BinomialNode *findNewExtreme(Binomial *bino) {
	BinomialNode *curr;
	bino->extreme = NULL;

	for(int i = 0; i < sizeDArray(bino->rootlist); i++) {
		curr = getDArray(bino->rootlist, i);
		if(curr){
			if(bino->extreme) {
				if(bino->compare(curr->value, bino->extreme->value) < 0)
					bino->extreme = curr;
			}
			else bino->extreme = curr;
		}
	}

	return bino->extreme;
}


void printRowInfo(FILE *fp, int rowcount) {
	if(rowcount == 0) fprintf(fp, "%d:", rowcount);
	else fprintf(fp, "\n%d:", rowcount);
}

void addRootToQueue(queue *q, BinomialNode *binode) {
	enqueue(q, binode);
	enqueue(q, NULL);
}


/* Public Variables */

/* Public Interface Implementations */

BinomialNode *newBinomialNode(void (*display)(FILE *, void *), void *value) {
	BinomialNode *newbinode;
	if((newbinode = malloc(sizeof(BinomialNode))) == 0)
		fatalBinomial("Binomimal node not allocated");

	newbinode->value = value;
	newbinode->children = newDArray(NULL);
	newbinode->display = display;
	newbinode->parent = NULL;

	return newbinode;
}

void displayBinomialNode(FILE *fp, BinomialNode *binode) {
	fprintf(fp, " ");
	binode->display(fp, binode->value);
	fprintf(fp, "-%d", sizeDArray(binode->children));

	if(binode->parent != binode) {
		fprintf(fp, "(");
		binode->display(fp, binode->parent->value);
		fprintf(fp, "-%d)", sizeDArray(binode->parent->children));
	}
}

Binomial *newBinomial(void (*display)(FILE *, void *),
					  int (*comparator)(void *, void *),
			          void (*updater)(void *, BinomialNode *)) {
	Binomial *newbino;
	if((newbino = malloc(sizeof(Binomial))) == 0)
		fatalBinomial("Binomial not allocated");

	newbino->rootlist = newDArray(NULL);
	newbino->extreme = NULL;
	newbino->size = 0;
	newbino->display = display;
	newbino->compare = comparator;
	newbino->update = updater;

	return newbino;
}

BinomialNode *insertBinomial(Binomial *bino, void *value) {
	BinomialNode *newbinode = newBinomialNode(bino->display, value);
	newbinode->parent = newbinode;

	if(!bino->extreme)
		bino->extreme = newbinode;
	else if(bino->compare(bino->extreme->value, newbinode->value) > 0)
		bino->extreme = newbinode;

	consolidate(bino, newbinode);

	bino->size++;

	return newbinode;
}

int sizeBinomial(Binomial *bino) {
	return bino->size;
}

void deleteBinomial(Binomial *bino, BinomialNode *binode) {
	decreaseKeyBinomial(bino, binode, NULL);
	extractBinomial(bino);
}

void decreaseKeyBinomial(Binomial *bino, BinomialNode *binode, void *value) {
	binode->value = value; // setting to null
	binode = bubbleUp(bino, binode);

	if(bino->compare(binode->value, bino->extreme->value) < 0)
		bino->extreme = binode;
}

void *extractBinomial(Binomial *bino) {
	int yindex;
	BinomialNode *y = bino->extreme;

	yindex = findIndexInRootlist(bino, y);
	setDArray(bino->rootlist, yindex, NULL);

	merge(bino, y->children);

	bino->size--;

	void *temp = y->value;
	bino->extreme = findNewExtreme(bino);

	return temp;
}

void displayBinomial(FILE *fp, Binomial *bino) {
	if(bino->size == 0) fprintf(fp, "0:\n");

	BinomialNode *binode, *child;
	queue *q = newQueue(bino->display);
	int rowcount = 0, j;

	for (int i = 0; i < sizeDArray(bino->rootlist); i++) {
		if (!(binode = getDArray(bino->rootlist, i))) continue;

		addRootToQueue(q, binode);
		printRowInfo(fp, rowcount++);
		while (1) {
			binode = dequeue(q);
			if (sizeQueue(q) == 0) break;

			if (binode) {
				displayBinomialNode(fp, binode);

				for (j = 0; j < sizeDArray(binode->children); j++)
					if ((child = getDArray(binode->children, j)))
						enqueue(q, child);
			} else {
				printRowInfo(fp, rowcount++);
				enqueue(q, NULL);
			}
		}
		rowcount = 0;
		fprintf(fp, "\n----\n");
	}
}
